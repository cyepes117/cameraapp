package company.cameraapp;

public class Constants {



    private Constants() {
    }

    public static final String TAG = "CameraApp";
    public static final String PATH = "Path";

    public static final String IMAGE_PREFIX = "IMG";
    public static final String IMAGE_SEPARATOR = "_";
    public static final String IMAGE_EXTENSION = ".jpg";

    public static final int IMAGE_SIZE = 650;
    public static final int IMAGE_ANIMATION_DURATION = 400;

    public static final int ROTATION_0 = 0;
    public static final int ROTATION_90 = 90;
    public static final int ROTATION_180 = 180;
    public static final int ROTATION_270 = 270;

    public static final int MEDIA_TYPE_IMAGE = 1;


    public static final int TOP_MARGIN = 10;
    public static final int BOTTOM_MARGIN = 10;
    public static final int LEFT_MARGIN = 20;
    public static final int RIGHT_MARGIN = 20;

}
