package company.cameraapp.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;
import java.util.List;

import company.cameraapp.Constants;
import company.cameraapp.R;
import company.cameraapp.utils.FileTools;
import company.cameraapp.utils.ImageTools;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout mLinearLayout;
    private Button mButton;
    private String mPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        /*
         * Init UI
         */
        mLinearLayout = (LinearLayout) findViewById(R.id.linear_photos);
        mButton = (Button) findViewById(R.id.button_add);
        mButton.setOnClickListener(this);

        /*
         * Init folder app
         */
        mPath = FileTools.createFolder(Environment.getExternalStorageDirectory().getAbsolutePath(), Constants.TAG);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mLinearLayout.removeAllViews();

        File directory = new File(mPath);

        for (File folder : directory.listFiles()) {
            List<Bitmap> images = ImageTools.getBitmapsFromFolder(folder);

            if (!images.isEmpty()) {
                ImageView view = new ImageView(this);

                /*
                 * Image layout settings
                 */
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(Constants.LEFT_MARGIN, Constants.TOP_MARGIN, Constants.RIGHT_MARGIN, Constants.BOTTOM_MARGIN);
                view.setLayoutParams(params);

                /*
                 * Add animation
                 */
                if (images.size() == 1) {
                    view.setImageBitmap(images.get(0));

                } else {
                    AnimationDrawable animation = new AnimationDrawable();

                    for (Bitmap image : images) {
                        BitmapDrawable frame = new BitmapDrawable(getResources(), image);
                        animation.addFrame(frame, Constants.IMAGE_ANIMATION_DURATION);
                    }

                    animation.setOneShot(false);
                    view.setBackgroundDrawable(animation);

                    animation.start();

                }

                mLinearLayout.addView(view);

            }


        }

    }

    @Override
    public void onClick(View view) {

        if (view == mButton) {
            Intent intent = new Intent(MainActivity.this, CameraActivity.class);
            intent.putExtra(Constants.PATH, mPath);
            startActivity(intent);
        }
    }
}