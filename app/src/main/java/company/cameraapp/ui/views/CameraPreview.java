package company.cameraapp.ui.views;

import android.content.Context;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

import company.cameraapp.ui.managers.CameraManager;
import company.cameraapp.Constants;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    private SurfaceHolder mHolder;
    private CameraManager mCameraManager = CameraManager.getInstance();

    public CameraPreview(Context context) {
        super(context);
        mCameraManager.setDisplayOrientation(Constants.ROTATION_90);

        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        mHolder = holder;
        try {
            mCameraManager.startPreview(mHolder);
        } catch (IOException e) {
            Log.d(Constants.TAG, "Error setting camera preview: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        if (mHolder.getSurface() != null) {
            try {
                mCameraManager.startPreview(mHolder);
            } catch (IOException e) {
                Log.d(Constants.TAG, "Error setting camera preview: " + e.getMessage());
            }
        }
    }
}