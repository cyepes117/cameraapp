package company.cameraapp.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import company.cameraapp.ui.managers.CameraManager;
import company.cameraapp.ui.views.CameraPreview;
import company.cameraapp.Constants;
import company.cameraapp.utils.FileTools;
import company.cameraapp.R;


public class CameraActivity extends Activity implements View.OnClickListener, Camera.PictureCallback {

    private CameraManager mCameraManager = CameraManager.getInstance();
    private Button mButtonPhoto;

    private String mPath;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.camera_activity);

        /*
         * Clean the view
         */
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        /*
         * Add customview
         */
        Camera camera = mCameraManager.getCamera();
        if (camera != null) {
            CameraPreview preview = new CameraPreview(this);
            FrameLayout framePreview = (FrameLayout) findViewById(R.id.camera_preview);
            framePreview.addView(preview);
        }

        /*
         * Set listeners
         */
        mButtonPhoto = (Button) findViewById(R.id.button_capture_photo);
        mButtonPhoto.setOnClickListener(this);

        /*
         * Crate folder
         */
        String folderApp = getIntent().getStringExtra(Constants.PATH);
        int length = FileTools.getFoldersLength(folderApp);
        mPath = FileTools.createFolder(folderApp, length + "");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        /*
         * Release camera
         */
        mCameraManager.releaseCamera();
    }


    @Override
    public void onClick(View v) {
        if (v == mButtonPhoto) {
            /*
             * Take photo
             */
            mCameraManager.takePicture(this);

        }

    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {

        File pictureFile = FileTools.getOutputMediaFile(Constants.MEDIA_TYPE_IMAGE, mPath);

        if (pictureFile != null) {
            try {
                FileTools.saveImage(data, pictureFile);
                Log.d(Constants.TAG, "File saved");

            } catch (FileNotFoundException e) {
                Log.e(Constants.TAG, "File not found: " + e.getMessage());

            } catch (IOException e) {
                Log.e(Constants.TAG, "Error accessing file: " + e.getMessage());

            }

        } else {
            Log.d(Constants.TAG, "Error creating media file, check storage permissions.");

        }

        /*
         * Start preview again
         */
        try {
            mCameraManager.startPreview(null);
        } catch (IOException e) {
            Log.e(Constants.TAG, "Cannot start preview: " + e.getMessage());
        }

    }
}
