package company.cameraapp.ui.managers;

import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;

import java.io.IOException;

import company.cameraapp.Constants;

public class CameraManager {

    private static CameraManager instance;
    private Camera mCamera;

    private CameraManager() {

    }

    public static CameraManager getInstance() {
        if (instance == null)
            instance = new CameraManager();
        return instance;
    }

    /*
     * Only get front camera
     */
    public Camera getCamera() {

        if (mCamera != null) {
            return mCamera;

        } else {
            Camera.CameraInfo info = new Camera.CameraInfo();

            for (int i = 0; i < Camera.getNumberOfCameras(); i++) {

                Camera.getCameraInfo(i, info);

                //only front camera is allowed
                if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    try {
                        mCamera = Camera.open(i);
                        break;
                    } catch (Exception e) {
                        Log.e(Constants.TAG, "Camera failed to open: " + e.getLocalizedMessage());
                    }
                }

            }
            return mCamera;
        }
    }

    /*
     * Release camera resource
     */
    public void releaseCamera() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    /*
     * Send message to listener when picture is taken
     */
    public void takePicture(Camera.PictureCallback listener) {
        if (mCamera != null) {
            mCamera.takePicture(null, null, listener);
        }
    }

    /*
     * Stop camera, set linked surface, start camera
     */
    public void startPreview(SurfaceHolder holder) throws IOException {
        if (mCamera != null) {
            mCamera.stopPreview();

            if (holder != null) {
                mCamera.setPreviewDisplay(holder);
            }

            mCamera.startPreview();
        }
    }

    public void setDisplayOrientation(int orientation) {
        if (mCamera != null) {
            mCamera.setDisplayOrientation(orientation);
        }
    }
}