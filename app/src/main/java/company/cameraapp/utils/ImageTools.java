package company.cameraapp.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import company.cameraapp.Constants;

public class ImageTools {

    private ImageTools() {
    }

    public static List<Bitmap> getBitmapsFromFolder(File folder) {
        List<Bitmap> response = new ArrayList<Bitmap>();

        if (folder.isDirectory()) {
            File[] files = folder.listFiles();
            for (File file : files) {
                if (!file.isDirectory()) {
                    Uri uri = Uri.fromFile(file);
                    Bitmap bitmap = getResizedBitmapFromUri(uri);
                    if (bitmap != null)
                        response.add(bitmap);
                }
            }

        }

        return response;
    }

    public static Bitmap getResizedBitmapFromUri(Uri path) {
        ExifInterface exif = null;

        try {
            exif = new ExifInterface(path.getPath());
        } catch (IOException e1) {
            e1.getMessage();
        }

        int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        int rotationInDegrees = exifToDegrees(rotation);

        Bitmap bitmap = resizeFile(new File(path.getPath()));

        if (rotation != 0f) {
            return rotateBitmap(bitmap, rotationInDegrees);
        } else {
            return bitmap;
        }
    }

    private static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return Constants.ROTATION_90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return Constants.ROTATION_180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return Constants.ROTATION_270;
        }
        return Constants.ROTATION_0;
    }

    private static Bitmap resizeFile(File f) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // The new size we want to scale to
            final int REQUIRED_SIZE = Constants.IMAGE_SIZE;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

}
