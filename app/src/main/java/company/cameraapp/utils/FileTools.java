package company.cameraapp.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import company.cameraapp.Constants;

public class FileTools {

    private FileTools() {
    }

    public static String createFolder(String path, String name) {

        File folder = new File(path + File.separator + name);

        if (!folder.exists()) {
            folder.mkdirs();
        }

        return folder.getAbsolutePath();
    }

    public static int getFoldersLength(String path) {
        File folder = new File(path);
        if (folder.isDirectory()) {
            File[] files = folder.listFiles();
            return files.length;
        }
        return 0;
    }

    public static File getOutputMediaFile(int type, String path) {

        File folder = new File(path);
        File mediaFile = null;

        if (type == Constants.MEDIA_TYPE_IMAGE) {
            mediaFile = new File(path + File.separator +
                    Constants.IMAGE_PREFIX + Constants.IMAGE_SEPARATOR + folder.listFiles().length + Constants.IMAGE_EXTENSION);
        }

        return mediaFile;
    }

    public static void saveImage(byte[] data, File pictureFile) throws IOException, FileNotFoundException {

        FileOutputStream fos = new FileOutputStream(pictureFile);
        fos.write(data);
        fos.close();

    }

}
